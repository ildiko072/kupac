import React from "react";
import Animation from '../Pages/Animation';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from "enzyme"
import { SwapNodesAnimationProvider } from '../binary-tree/contexts/SwapNodesAnimation';

let wrapper = null;
let WrapperComponent = null;

jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useLocation: () => ({
        pathname: "localhost:3000/#/bemutato"
    })
}));


beforeEach(() => {
    configure({ adapter: new Adapter() });
    WrapperComponent = () => (
        <SwapNodesAnimationProvider>
            <Animation />
        </SwapNodesAnimationProvider>
    );
    wrapper = mount(<WrapperComponent />);

    // jest.mock("react-router-dom", () => ({
    //     ...jest.requireActual("react-router-dom"),
    //     useLocation: () => ({
    //         pathname: "localhost:3000/#/bemutato"
    //     })
    // }));
});

afterEach(() => {
    // cleanup
    wrapper.unmount();
});

it("Tree renders empty when opening the page", () => {
    expect(wrapper.find('.ui.placeholder.segment').first().prop('children')).toBe(" Fa generálásához nyomd meg a 'Generál' gombot! ");
});

it("Tree is generated when 'General' button is hit", () => {
    // wrapper.find('button').at(4).simulate('click');
    wrapper.findWhere((node) => node.prop('children') === 'Generál').simulate('click');

    expect(wrapper.find({ singleLine: true })).toHaveLength(1)
    expect(wrapper.find({ nodeType: 'root' })).toHaveLength(1)
    
});
