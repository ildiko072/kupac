import React from "react";
import Practise from '../Pages/Practise';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from "enzyme"
import { SwapNodesPractiseProvider } from '../binary-tree/contexts/SwapNodesPractise';

let wrapper = null;
let WrapperComponent = null;

jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useLocation: () => ({
        pathname: "localhost:3000/#/gyakorlas"
    })
}));


beforeEach(() => {
    configure({ adapter: new Adapter() });
    WrapperComponent = () => (
        <SwapNodesPractiseProvider>
            <Practise />
        </SwapNodesPractiseProvider>
    );
    wrapper = mount(<WrapperComponent />);
});

afterEach(() => {
    // cleanup
    wrapper.unmount();
});

it("Tree renders empty when opening page", () => {
    expect(wrapper.find('.ui.placeholder.segment').first().prop('children')).toBe(" Fa generálásához nyomd meg a 'Kezdés' gombot, vagy építs egy saját fát! ");
});

it("Tree is generated when 'Kezdes' button is hit", () => {
    // wrapper.find('button').at(4).simulate('click');
    wrapper.findWhere((node) => node.prop('children') === 'Kezdés').simulate('click');

    expect(wrapper.find({ singleLine: true })).toHaveLength(1)
    expect(wrapper.find({ nodeType: 'root' })).toHaveLength(1)
    
});

it("Help message is displayed when 'Segitseg' button is hit", () => {
    wrapper.findWhere((node) => node.prop('children') === 'Segítség').simulate('click')

    expect(wrapper.find('#actionMessage').at(0).text()).toContain("Ezt a két elemet cseréld meg:")
    
});

// it("When mode is 'Kupacrendezes' the program generates a maximum heap", () => {
//     wrapper.findWhere((node) => node.prop('children') === 'Kupacrendezés').simulate('click')
//     wrapper.findWhere((node) => node.prop('children') === 'Kezdés').simulate('click')

//     let previousNode = 100;

//     wrapper.find({ singleLine: true }).find('td').forEach((node) => {
//         console.log(parseInt(node.text()))
//         expect(parseInt(node.text())).toBeLessThanOrEqual(previousNode)
//     })
// })
