import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

export default class MenuPointing extends Component {
  state = { activeItem: '' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Menu color='teal'>
        <Menu.Item
          name='concept'
          active={activeItem === 'concept'}
          onClick={this.handleItemClick}
          as={NavLink} to="/elmelet"
        >
          Elmélet
        </Menu.Item>
        <Menu.Item
          name='animation'
          active={activeItem === 'animation'}
          onClick={this.handleItemClick}
          as={NavLink} to="/bemutato"
        >
          Bemutató
        </Menu.Item>
        <Menu.Item
          name='practise'
          active={activeItem === 'practise'}
          onClick={this.handleItemClick}
          as={NavLink} to="/gyakorlas"
        >
          Gyakorlás
        </Menu.Item>
      </Menu>
    )
  }
}
