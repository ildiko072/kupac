import React from 'react'
import { Table } from 'semantic-ui-react'

const BuildMaxHeapAlgorithm = (props) => {
  let sinkRow = null;
  if (props.noColor) {
    sinkRow = <Table.Cell>süllyeszt(A,k,n)</Table.Cell>
  } else {
    sinkRow = <Table.Cell error={true}>süllyeszt(A,k,n)</Table.Cell>
  }

  return (
    <Table celled  textAlign='center'>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell colSpan="2">kupacol (A/1 : T[n])</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        <Table.Row>
          <Table.Cell colSpan='2'>
            k := szülő(n) {'>'}= 1
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell rowSpan='2' className="topBorder"/>
          {sinkRow}
        </Table.Row>
      </Table.Body>

    </Table>
  );
}

export default BuildMaxHeapAlgorithm
