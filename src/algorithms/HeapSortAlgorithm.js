import React from 'react'
import { Table } from 'semantic-ui-react'

const HeapSortAlgorithm = (props) => {
  let swapComp = <Table.Cell>cserél(A[1], A[m])</Table.Cell>;
  if (props.inHeapSortMode && !props.inSinkModeInSort) {
    swapComp = <Table.Cell error='true'>cserél(A[1], A[m]</Table.Cell>;
  }

  let sinkComp = <Table.Cell className="leftBorder">süllyeszt(A,1,m)</Table.Cell>;
  if (props.inSinkModeInSort) {
    sinkComp = <Table.Cell error={true} className="leftBorder">süllyeszt(A,1,m)</Table.Cell>;
  }

  return (
    <Table celled textAlign='center'>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell colSpan="2">kupacrendezés (A/1 : T[n])</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        <Table.Row>
          <Table.Cell colSpan='2'>
            kupacol(A)
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell colSpan="2">m := n</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell colSpan="2">m {'>'} 1</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell rowSpan="3" className="topBorder"> </Table.Cell>
          {swapComp}
        </Table.Row>
        <Table.Row>
          <Table.Cell className="leftBorder">m := m-1</Table.Cell>
        </Table.Row>
        <Table.Row>
          {sinkComp}
        </Table.Row>
      </Table.Body>
    </Table>
  );
}

export default HeapSortAlgorithm
