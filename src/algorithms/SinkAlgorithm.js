import React from 'react'
import { Table } from 'semantic-ui-react'

const SinkAlgorithm = (props) => {
  let swapComp = <Table.Cell className="leftBorder">csere(A[i], A[j])</Table.Cell>;
  let noSwapComp = <Table.Cell rowSpan='2'>b := hamis</Table.Cell>;
  let compareComponent = <Table.Cell colSpan="2" className="leftBorder">{'A[i] < A[j]'}</Table.Cell>;
  let commentForCompareComponent = <Table.Cell colSpan="2" className="leftBorder">{'// A[j] a nagyobb gyereke A[i]-nek'}</Table.Cell>;
  
  if(props.compareMode) {
    compareComponent = <Table.Cell colSpan="2" error={true} className="leftBorder">{'A[i] < A[j]'}</Table.Cell>;
    commentForCompareComponent = <Table.Cell colSpan="2" error={true} className="leftBorder">{'// A[j] a nagyobb gyereke A[i]-nek'}</Table.Cell>;
  } else if (props.noSinkNeeded) {
    noSwapComp = <Table.Cell rowSpan='2' error={true}>b := hamis</Table.Cell>;
  } else if (props.noColor) {
    swapComp = <Table.Cell className="leftBorder">csere(A[i], A[j])</Table.Cell>;
  } else {
    swapComp = <Table.Cell className="leftBorder" error={true}>csere(A[i], A[j])</Table.Cell>;
  }

  return (
    <Table celled  textAlign='center'>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell colSpan="3">süllyeszt (A/1 : T[n]; k, n : N)</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        <Table.Row>
          <Table.Cell colSpan='3'>
            i := k; j := bal(k); b := igaz
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell colSpan="3">j {'<='} n && b </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell rowSpan='8' className="topBorder"></Table.Cell>
          <Table.Cell colSpan="2">{'// A[j] a bal gyereke A[i]-nek'}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell colSpan="2" className="leftBorder">{'j < n && A[j+1] > A[j]'}</Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell className="leftBorder">j := j+1</Table.Cell>
          <Table.Cell>SKIP</Table.Cell>
        </Table.Row>
        <Table.Row>
          {commentForCompareComponent}
        </Table.Row>
        <Table.Row>
          {compareComponent}
        </Table.Row>
        <Table.Row>
          {swapComp}
          {noSwapComp}
        </Table.Row>
        <Table.Row>
          <Table.Cell className="leftBorder">i := j; j := bal(j)</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  )
}

export default SinkAlgorithm
