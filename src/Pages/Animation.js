import React, { useContext, useState } from 'react';
import { Button, Table, Grid, Segment, Popup } from 'semantic-ui-react';
import { SwapNodesAnimation } from '../binary-tree/contexts/SwapNodesAnimation';
import "../css/BinaryNodeTreeApp.css";
import "bootstrap/dist/css/bootstrap.min.css";
import BinarySearchTreeNode from '../binary-tree/components/BinarySearchTreeNode.js';
import BinarySearchTreeNodeMeta from '../binary-tree/classes/BinarySearchTreeNodeMeta.js';
import HeapSortAlgorithm from '../algorithms/HeapSortAlgorithm.js';
import BuildMaxHeapAlgorithm from '../algorithms/BuildMaxHeapAlgorithm.js';
import SinkAlgorithm from '../algorithms/SinkAlgorithm';
import ActionMessage from '../messages/ActionMessage';
import { useLocation } from 'react-router-dom';

let currentParent = null;
let root = null;
let tree = [];
let nodeIndex = 0;
let iValue = null;
let nextWasHit = false;
let maxHeapReady = false;

const messageTexts = {
  startMaxHeap: 'A maximum kupac építését az utolsó levél elemtől kezdjük. Összehasonlítjuk a levelekhez tartozó csonka kupac elemeit.',
  compare: 'Összehasonlítjuk a következő csonka kupac elemeit.',
  rootIsSmallerSwap: 'Csere: A levél csúcs kulcsa nagyobb mint a gyökéré, így megcseréljük.',
  rootIsBigger: 'Nincs csere: A levél elem(ek) kulcsa kisebb, mint a gyökéré így nem cserélünk.',
  endOfMaxHeap: 'Kialakítottuk a maximum kupacot.',
  startHeapSort: 'Legnagyobb elem kiválasztásával kezdünk.',
  swapNextBiggest: 'A következő legnagyobb elem kiválasztása.',
  sinkSwapped : 'A cserélt elemet lesüllyesztjük, ha szükséges.',
  sinkToHeapify : 'A cserélt elemet süllyesztjük, hogy a kupac tulajdonság helyreálljon.',
  noMoreSink: 'A levél elem(ek) nem nagyobb(ak), mint a süllyesztett, így nem süllyesztünk tovább.',
  itemInPlace: 'A legnagyobb elem a helyére került, így lezárjuk.',
  heapSortReady: 'A rendezés befejeződött.'
};

const Animation = (props) => {
  const [insertValue, setInsertValue] = useState("");
  const [actionText, setActionText] = useState("");
  const [actionTextVisible, setActionTextVisible] = useState(false);
  const [nextAvailable, setNextAvailable] = useState(false);
  const [modeIsHeapify, setModeIsHeapify] = useState(true);
  const [modeIsAuto, setModeIsAuto] = useState(false);
  const [inBuildingMaxHeapMode, setInBuildingMaxHeapMode] = useState(true);
  const [inSinkMode, setInSinkMode] = useState(false);
  const [noSinkNeeded, setNoSinkNeeded] = useState(false);
  const [compareMode, setCompareMode] = useState(false);
  const [inHeapSortMode, setInHeapSortMode] = useState(false);
  const [workInProgress, setWorkInProgress] = useState(false);
  const nodesTobeSwapped = useContext(SwapNodesAnimation);
  const [key, setKey] = React.useReducer(c => c+1, 0);
  const location = useLocation();

  let thirdNode;
  let s1, s2;

  const sleep = async (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
  };

  useState(() => {
    if (tree.length > 3) window.location.reload();
  }, [location]);

  // Stop animation (Bemutato leallitasa)
  const stopHit = () => {
    window.location.reload();
  }

  // Waiting for Next button to be hit, calling itself until it was hit
  // (Kovetkezo megnyomasara varas, meghivja onmagat amig a gomb lenyomas teljesul)
  const resolveNext = async () => {
      const flag = resolve => {
        if (nextWasHit) {
          nextWasHit = false;
          resolve();
        } else {
          setTimeout(_ => flag(resolve), 1000);
        }
      }
      return new Promise(flag);
  };

  // Validating value and insert 
  // (Beszurando elem ellenorzese és beszuras meghivasa)
  const insert = () => {
      if ( insertValue !== '' || iValue !== null) {
      const newNode = insertValue !=='' ? new BinarySearchTreeNodeMeta(insertValue, nodeIndex)
        :new BinarySearchTreeNodeMeta(iValue, nodeIndex);
      if (root === null) {
        root = newNode;
        currentParent = newNode;
      }
      else {
        insertNode(root, newNode);
      }
      tree = tree.concat(newNode);
      setInsertValue("");
      nodeIndex = nodeIndex + 1;
    }
  };

  // (Elem beszurasa a faba)
  const insertNode = (node, newNode) => {
   if (currentParent.left === null) {
      currentParent.setLeft(newNode);
    } else if (currentParent.right === null) {
      currentParent.setRight(newNode); 
    
      if (node.height() > 1 ) {
        if(currentParent.parent.leftHeight() > currentParent.parent.rightHeight()) {
          currentParent = currentParent.parent.right;
          while(currentParent.left) {
            currentParent = currentParent.right;
          }
          return
        } else {
          currentParent = node.left;
          while(currentParent.left) {
            currentParent = currentParent.left;
          }
        }
        
        if (node.leftHeight() > node.rightHeight()) {
          currentParent = node.right;
          while(currentParent.left) {
            currentParent = currentParent.left;
          }
        }
      } else {
          currentParent = node.left;
          while(currentParent.left) {
            currentParent = currentParent.left;
          }
      }
    }
  };

  // (Animacio es csere)
  const findAndSwap = () => {
    const temp = nodesTobeSwapped.swap1.value;
    nodesTobeSwapped.swap1.value = nodesTobeSwapped.swap2.value;
    nodesTobeSwapped.swap2.value = temp;
    nodesTobeSwapped.swap1.state = 'inSwap';
    nodesTobeSwapped.swap2.state = 'inSwap';
    nodesTobeSwapped.swap1 = null;
    nodesTobeSwapped.swap2 = null;
  };

  // (Ket elem csereje)
  const swapNodes = () => {
    deActivate();
    if(nodesTobeSwapped.swap1 && nodesTobeSwapped.swap2) {
      findAndSwap();
      setKey();
    }
  }

  // Clearing elements state (Elemek alap allapotba helyezese, kiveve a mar lezartat)
  const deActivate = () => {
    tree.forEach(element => {if(element.state !== 'inPlace') element.state = ''});
  }

  // Auto animated sinking (Sullyesztes automatikus animalassal)
  const sink = async (index, lastIndex) => {
    await sleep(3000);
    let i = index;
    let j = tree[index].left && tree[index].left.index;
    let b = true;

    while (j && j <= lastIndex && b) {
      setInBuildingMaxHeapMode(false);
      setInSinkMode(true);
      setNoSinkNeeded(false);
      if(j < lastIndex && tree[j+1].value > tree[j].value) {
        j = j + 1;
      }
      nodesTobeSwapped.swap1 = tree[i];
      nodesTobeSwapped.swap2 = tree[j];
      nodesTobeSwapped.swap1.state = 'active';
      nodesTobeSwapped.swap1.left.state = 'active';
      if(nodesTobeSwapped.swap1.right && nodesTobeSwapped.swap1.right.state !== 'inPlace') nodesTobeSwapped.swap1.right.state = 'active';
      setCompareMode(true);
      if (maxHeapReady) {
        setActionText(messageTexts.sinkToHeapify);
      } else {
        setActionText(messageTexts.sinkSwapped);
      }
      setKey();
      await sleep(2000);
      if (tree[i].value < tree[j].value) {
        setCompareMode(false);
        swapNodes();
        await sleep(2000);
        deActivate();
        i = j;
        j = tree[j].left ? tree[j].left.index : null;
      } else {
        b = false;
        setCompareMode(false);
        setNoSinkNeeded(true);
        deActivate();
      }
    }
    setCompareMode(false);
    setNoSinkNeeded(false);
    setInSinkMode(false);
    setInBuildingMaxHeapMode(true);
  }

  // (Sullyesztes animalasa lepesenkent)
  const sinkInSteps = async (index, lastIndex) => {
    let i = index;
    let j = tree[index].left && tree[index].left.index;
    let b = true;

    while (j && j <= lastIndex && b) {
      setNoSinkNeeded(false);
      setInSinkMode(true);
      if(j < lastIndex && tree[j+1].value > tree[j].value) {
        j = j + 1;
      }
      nodesTobeSwapped.swap1 = tree[i];
      nodesTobeSwapped.swap2 = tree[j];
      nodesTobeSwapped.swap1.state = 'active';
      nodesTobeSwapped.swap1.left.state = 'active';
      if(nodesTobeSwapped.swap1.right && nodesTobeSwapped.swap1.right.state !== 'inPlace') nodesTobeSwapped.swap1.right.state = 'active';
      setCompareMode(true);
      if (maxHeapReady) {
        setActionText(messageTexts.sinkToHeapify);
      } else {
        setActionText(messageTexts.sinkSwapped);
      }
      setKey();
      await resolveNext((flag) => flag === true);
      if (tree[i].value < tree[j].value) {
        setCompareMode(false);
        swapNodes();
        await resolveNext((flag) => flag === true);
        deActivate();
        i = j;
        j = tree[j].left ? tree[j].left.index : null;
      } else {
        b = false;
        setCompareMode(false);
        setNoSinkNeeded(true);
        setActionText(messageTexts.noMoreSink);
        await resolveNext((flag) => flag === true);
        deActivate();
      }
    }
    setCompareMode(false);
    setNoSinkNeeded(false);
    setInSinkMode(false);
  }

  // Max heap building auto animation (Maximum kupac epitese automatikus animacioval) 
  const buildMaxHeap = async (index) => {
    setWorkInProgress(true);
    setInBuildingMaxHeapMode(true);
    nodesTobeSwapped.swap1 = tree[index];
    let switchedIndex = null;
    nodesTobeSwapped.swap2 = nodesTobeSwapped.swap1.parent;
    if(nodesTobeSwapped.swap2.left !== nodesTobeSwapped.swap1) {
      thirdNode = nodesTobeSwapped.swap2.left
      if(nodesTobeSwapped.swap1.value > nodesTobeSwapped.swap2.value) {
        switchedIndex = nodesTobeSwapped.swap1.index;
        if(thirdNode.value > nodesTobeSwapped.swap1.value) {
          nodesTobeSwapped.swap1 = thirdNode;
          switchedIndex = nodesTobeSwapped.swap1.index;
        }
        setActionText(messageTexts.compare);
        nodesTobeSwapped.swap2.state = 'active';
        nodesTobeSwapped.swap2.left.state = 'active';
        nodesTobeSwapped.swap2.right.state = 'active';
        setKey();
        await sleep(3000);
        setActionText(messageTexts.rootIsSmallerSwap);
        swapNodes();
        await sleep(2000);
        deActivate();
        await sink(switchedIndex, tree.length-1);
      } else {
        if(thirdNode.value > nodesTobeSwapped.swap2.value) {
          nodesTobeSwapped.swap1 = thirdNode;
          switchedIndex = nodesTobeSwapped.swap1.index;
          setActionText(messageTexts.compare);
          nodesTobeSwapped.swap2.state = 'active';
          nodesTobeSwapped.swap2.left.state = 'active';
          nodesTobeSwapped.swap2.right.state = 'active';
          setKey();
          await sleep(3000);
          setActionText(messageTexts.rootIsSmallerSwap);
          swapNodes();
          await sleep(2000);
          deActivate();
          await sink(switchedIndex, tree.length-1);
        } else {
          // only animate, no change needed
          setActionText(messageTexts.compare);
          nodesTobeSwapped.swap2.state = 'active';
          nodesTobeSwapped.swap2.left.state = 'active';
          nodesTobeSwapped.swap2.right.state = 'active';
          setKey();
          await sleep(3000);
          setActionText(messageTexts.rootIsBigger);
          await sleep(2000);
          deActivate();
        }
      }
    } else {
      if(nodesTobeSwapped.swap1.value > nodesTobeSwapped.swap2.value) {
        switchedIndex = nodesTobeSwapped.swap1.index;
        setActionText(messageTexts.rootIsSmallerSwap);        
        nodesTobeSwapped.swap1.state = 'active';
        nodesTobeSwapped.swap2.state = 'active';
        setKey();
        await sleep(2000);
        swapNodes();
        await sleep(2000);
        deActivate();
        await sink(switchedIndex, tree.length-1);
      } else {
        // only animate, no change needed
        nodesTobeSwapped.swap1.state = 'active';
        nodesTobeSwapped.swap2.state = 'active';
        setKey();
        await sleep(3000);
        setActionText(messageTexts.rootIsBigger);
        deActivate();
      }
    }
    await sleep(3000);
    if (index > 2) {
      if (index%2 === 0) {
        buildMaxHeap(index-2);
      } else {
        buildMaxHeap(index-1);
      }
    } else {
      setActionText(messageTexts.endOfMaxHeap)
      deActivate();
      nodesTobeSwapped.swap1 = null;
      nodesTobeSwapped.swap2 = null;
      maxHeapReady = true;
      setKey();
      setWorkInProgress(false);
    }
  }

  // Maximum heap building in steps (Kupac kialakitasa lepesenkent animalva)
  const buildMaxHeapInSteps = async (index) => {
    setInBuildingMaxHeapMode(true);
    nodesTobeSwapped.swap1 = tree[index];
    let switchedIndex = null;
    nodesTobeSwapped.swap2 = nodesTobeSwapped.swap1.parent;
    if(nodesTobeSwapped.swap2.left !== nodesTobeSwapped.swap1) {
      thirdNode = nodesTobeSwapped.swap2.left
      if(nodesTobeSwapped.swap1.value > nodesTobeSwapped.swap2.value) {
        switchedIndex = nodesTobeSwapped.swap1.index;
        if(thirdNode.value >= nodesTobeSwapped.swap1.value) {
          nodesTobeSwapped.swap1 = thirdNode;
          switchedIndex = nodesTobeSwapped.swap1.index;
        }
        setActionText(messageTexts.compare);
        nodesTobeSwapped.swap2.state = 'active';
        nodesTobeSwapped.swap2.left.state = 'active';
        nodesTobeSwapped.swap2.right.state = 'active';
        setKey();
        await resolveNext((flag) => flag === true);
        setActionText(messageTexts.rootIsSmallerSwap);
        swapNodes();
        deActivate();
        await resolveNext((flag) => flag === true);
        setInBuildingMaxHeapMode(false);
        await sinkInSteps(switchedIndex, tree.length-1)
      } else {
        if(thirdNode.value > nodesTobeSwapped.swap2.value) {
          nodesTobeSwapped.swap1 = thirdNode;
          switchedIndex = nodesTobeSwapped.swap1.index;
          setActionText(messageTexts.compare);
          nodesTobeSwapped.swap2.state = 'active';
          nodesTobeSwapped.swap2.left.state = 'active';
          nodesTobeSwapped.swap2.right.state = 'active';
          setKey();
          await resolveNext((flag) => flag === true);
          setActionText(messageTexts.rootIsSmallerSwap);
          swapNodes();
          deActivate();
          await resolveNext((flag) => flag === true);
          setInBuildingMaxHeapMode(false);
          await sinkInSteps(switchedIndex, tree.length-1);
        } else {
          await resolveNext((flag) => flag === true);
          // only animate, no change needed
          setActionText(messageTexts.compare);
          nodesTobeSwapped.swap2.state = 'active';
          nodesTobeSwapped.swap2.left.state = 'active';
          nodesTobeSwapped.swap2.right.state = 'active';
          setKey();
          await resolveNext((flag) => flag === true);
          setActionText(messageTexts.rootIsBigger);
          
          await resolveNext((flag) => flag === true);
          deActivate();
        }
      }
    } else {
      if(nodesTobeSwapped.swap1.value > nodesTobeSwapped.swap2.value) {
        switchedIndex = nodesTobeSwapped.swap1.index;
        setActionText(messageTexts.compare);
        nodesTobeSwapped.swap1.state = 'active';
        nodesTobeSwapped.swap2.state = 'active';
        setKey();
        await resolveNext((flag) => flag === true);
        setActionText(messageTexts.rootIsSmallerSwap);
        swapNodes();
        deActivate();
        await resolveNext((flag) => flag === true);
        setInBuildingMaxHeapMode(false);
        await sinkInSteps(switchedIndex, tree.length-1);
      } else {
        // only animate, no change needed
        nodesTobeSwapped.swap1.state = 'active';
        nodesTobeSwapped.swap2.state = 'active';
        setKey();
        await resolveNext((flag) => flag === true);
        setActionText(messageTexts.rootIsBigger);
        await resolveNext((flag) => flag === true);
        deActivate();
      }
    }
    if (index > 2) {
      if (index%2 === 0) {
        buildMaxHeapInSteps(index-2);
      } else {
        buildMaxHeapInSteps(index-1);
      }
    } else {
      setActionText(messageTexts.endOfMaxHeap)
      deActivate();
      nodesTobeSwapped.swap1 = null;
      nodesTobeSwapped.swap2 = null;
      maxHeapReady = true;
      setNextAvailable(false);
      setKey();
      setWorkInProgress(false);
    }
  }

  // Swapping for generated max heap building (Csere a generalt max kupachoz)
  const swapNodesGenerate = () => {
    if (s1 && s2) {
      const temp = s1.value;
      s1.value = s2.value;
      s2.value = temp;
      s1 = null;
      s2 = null
    }
  }

  // Sinking item for generated maximum heap building (Sullyesztes a generalt max kupachoz)
  const sinkInGenerate = (index, lastIndex) => {
    let i = index;
    let j = tree[index].left && tree[index].left.index;
    let b = true;

    while (j && j <= lastIndex && b) {
      if (j < lastIndex && tree[j + 1].value > tree[j].value) {
        j = j + 1;
      }
      s1 = tree[i];
      s2 = tree[j];
      if (tree[i].value < tree[j].value) {
        swapNodesGenerate();
        i = j;
        j = tree[j].left ? tree[j].left.index : null;
      } else {
        b = false;
      }
    }
  }

  // Generated max heap building (Generalt maximum kupac epitese)
  const buildGeneratedMaxHeap = (index) => {
    s1 = tree[index];
    let switchedIndex = null;
    let thirdNode = null;
    s2 = s1.parent;
    if (s2.left !== s1) {
      thirdNode = s2.left
      if (s1.value > s2.value) {
        switchedIndex = s1.index;
        if (thirdNode.value > s1.value) {
          s1 = thirdNode;
          switchedIndex = s1.index;
        }
        swapNodesGenerate();
        sinkInGenerate(switchedIndex, tree.length - 1);
      } else {
        if (thirdNode.value > s2.value) {
          s1 = thirdNode;
          switchedIndex = s1.index;
          swapNodesGenerate();
          sinkInGenerate(switchedIndex, tree.length - 1);
        }
      }
    } else {
      if (s1.value > s2.value) {
        switchedIndex = s1.index;
        swapNodesGenerate();
        sinkInGenerate(switchedIndex, tree.length - 1);
      }
    }
    if (index > 2) {
      if (index % 2 === 0) {
        buildGeneratedMaxHeap(index - 2);
      } else {
        buildGeneratedMaxHeap(index - 1);
      }
    } else {
      s1 = null;
      s2 = null;
      maxHeapReady = true;
    }
  }

  // Heapsort step by step animation (kupacrendezes lepesenkenti lejatszas)
  const heapSortInSteps = async () => {
    setModeIsHeapify(false);
    setActionTextVisible(true);
    setNextAvailable(true);
    setWorkInProgress(true);
    setActionText(messageTexts.startHeapSort);
    let m = tree.length-1;
    while (m > 0) {
      setInHeapSortMode(true);
      setActionText(messageTexts.swapNextBiggest);
      nodesTobeSwapped.swap1 = tree[0];
      nodesTobeSwapped.swap2 = tree[m];
      nodesTobeSwapped.swap1.state = 'active';
      nodesTobeSwapped.swap2.state = 'active';
      setKey();
      await resolveNext((flag) => flag === true);
      swapNodes();
      deActivate();
      tree[m].state ='inPlace';
      await resolveNext((flag) => flag === true);
      setActionText(messageTexts.itemInPlace);
      await resolveNext((flag) => flag === true);
      setKey();
      m = m-1;
      await sinkInSteps(0, m);
    }
    tree[0].state ='inPlace';
    setActionText(messageTexts.heapSortReady);
    setNextAvailable(false);
    await sleep(3000);
    setWorkInProgress(false);
  }

  //Heapsort animation in auto mode (kupacrendezes automatikus lejatszas)
  const heapSort = async () => {
    setModeIsHeapify(false);
    setActionTextVisible(true);
    setWorkInProgress(true);
    setActionText(messageTexts.startHeapSort);
    let m = tree.length-1;
    while (m > 0) {
      setInHeapSortMode(true);
      setActionText(messageTexts.swapNextBiggest);
      nodesTobeSwapped.swap1 = tree[0];
      nodesTobeSwapped.swap2 = tree[m];
      nodesTobeSwapped.swap1.state = 'active';
      nodesTobeSwapped.swap2.state = 'active';
      setKey();
      await sleep(2000);
      swapNodes();
      deActivate();
      await sleep(2000);
      tree[m].state ='inPlace';
      setActionText(messageTexts.itemInPlace);
      await sleep(1500);
      setKey();
      m = m-1;
      await sink(0, m);
    }
    tree[0].state ='inPlace';
    setActionText(messageTexts.heapSortReady);
    await sleep(3000);
    setWorkInProgress(false);
  }

  const generateTree = () => {
    deleteTree();
    maxHeapReady = false;
    setActionTextVisible(false);
    let i = Math.floor(Math.random() * 12) + 4;
    setKey();
    while (i > 0) {
      iValue = (Math.floor(Math.random() * 100));
      insert();
      i--;
    }
    // dummyTree();

    if(!modeIsHeapify) {
      buildGeneratedMaxHeap(tree.length-1);
    }
  }

  const start = async() => {
    if (modeIsAuto) {
      if (modeIsHeapify) {
        setWorkInProgress(true);
        setActionTextVisible(true);
        setActionText(messageTexts.startMaxHeap);
        let index = tree.length-1;
        await sleep(2500);
        buildMaxHeap(index);
      } else {
        heapSort();
      }
    } else {
      if (modeIsHeapify) {
        setWorkInProgress(true);
        setNextAvailable(true);
        setActionTextVisible(true);
        setActionText(messageTexts.startMaxHeap);
        let index = tree.length-1;
        await sleep(2500);
        buildMaxHeapInSteps(index);
      } else {
        heapSortInSteps();
      }
    }
  }

  // const dummyTree = () => {
  //   deleteTree();
  //   let t = [9, 21, 69, 54, 89, 81, 18, 37, 59, 11, 28, 3, 34, 37];
  //   setKey();
  //   t.forEach(e => {
  //     iValue = e;
  //     insert();
  //   })
  // }

  const deleteTree = () => {
    setKey();
    setActionTextVisible(false);
    maxHeapReady = false;
    // heapSortReady = false;
    nodesTobeSwapped.swap1 = null;
    nodesTobeSwapped.swap2 = null;
    root = null;
    tree = [];
    nodeIndex = 0;
  }

  const stepForward = () => {
    nextWasHit = true;
  }

  const setAutoMode = () => {
    setModeIsAuto(true);
  }

  const setStepsMode = () => {
    setModeIsAuto(false);
  }

  const setHeapifyMode = () => {
    setModeIsHeapify(true);
    deleteTree();
  }

  const setSortingMode = () => {
    setModeIsHeapify(false);
    if (!maxHeapReady) deleteTree();
  }


  // Style for array display
  let comparedCellStyle= { border: '1px solid black'}
  let switchedCellStyle= { border: '1px solid orange', color: 'orange'}
  let inPlaceCellStyle= { background: 'lightgrey'}

  let treeTable=[];

  /* eslint-disable */
  tree.map((data) => {
    switch(data.state) {
      case "active":
        treeTable.push(
          <Table.Cell key={data.index} style={comparedCellStyle} textAlign='center'>
          {data.value} 
          </Table.Cell>)
        break;
      case "inSwap":
        treeTable.push(
            <Table.Cell key={data.index} style={switchedCellStyle} textAlign='center'>
            {data.value} 
            </Table.Cell>)
        break;
      case "inPlace":
        treeTable.push(
            <Table.Cell key={data.index} style={inPlaceCellStyle} textAlign='center'>
            {data.value} 
            </Table.Cell>)
        break;
      default:
        treeTable.push(
          <Table.Cell key={data.index} textAlign='center'>
          {data.value} 
          </Table.Cell>)
    }
  })
  /* eslint-enable */

  let algorithmTable = null;
  let nextButton = null;
  let startEndButton = null;

  if (modeIsHeapify === true) {
    if (tree.length > 2 && inBuildingMaxHeapMode) {
      algorithmTable = <Segment><BuildMaxHeapAlgorithm /></Segment>;
    } else if (tree.length > 2 && inSinkMode) {
      algorithmTable = <Segment><SinkAlgorithm compareMode={compareMode} noSinkNeeded={noSinkNeeded}/></Segment>;
    }
  } else {
    if (tree.length > 2) {
      algorithmTable = <Segment><HeapSortAlgorithm inHeapSortMode={inHeapSortMode} inSinkModeInSort={inSinkMode} /></Segment>;
    }
  };

  if (!modeIsAuto) {
    nextButton = 
      <Segment>
        <div className="center">
          <Button
            basic
            color='purple'
            onClick={stepForward}
            id="action-button"
            disabled={!nextAvailable}
          >
            Következő
          </Button>
        </div>
      </Segment>
  }

  if (!workInProgress) {
    startEndButton =
      <div className="action">
        <Button
          basic
          color='orange'
          onClick={start}
          disabled={workInProgress ||  tree.length < 3}
          id='startButton'
        >
          Kezdés
        </Button>
      </div>
  } else {
    startEndButton = 
      <div className="action">
        <Button
          basic
          color='red'
          onClick={stopHit}
          id='stopButton'
        >
          Kilépés
        </Button>
      </div>
  }

  return (
    <React.Fragment>
      <Grid id="app" >
        <Grid.Column id="basic-actions" width='4'>
          <Segment.Group>
            <Segment className='actionSegment'>
              <div className="center">
                <div id="mode">Válassz animálási módot:</div>
                <Popup content='Lépésenkénti animálás' trigger={
                  <Button
                    toggle
                    active={!modeIsAuto}
                    onClick={setStepsMode}
                    className={"activeButton"}
                  >
                    Lépésenként
                  </Button>}
                />
              </div>
              <div className="center">
                <Popup content='Animálás automatikus léptetéssel' trigger={
                  <Button
                    toggle
                    active={modeIsAuto}
                    onClick={setAutoMode}
                    className={"activeButton"}
                  >
                    Automatikus
                  </Button>}
                />
              </div>
              <div style={{height: '10px'}}></div>
              <div className="center">
                <Button
                  toggle
                  active={modeIsHeapify}
                  onClick={setHeapifyMode}
                  disabled={workInProgress}
                  className={"activeButton"}
                >
                  Kupacol
                </Button>
              </div>
              <div className="center">
                <Button
                  toggle
                  active={!modeIsHeapify}
                  onClick={setSortingMode}
                  disabled={workInProgress}
                  className={"activeButton"}
                >
                  Kupacrendezés
                </Button>
              </div>
              <div className="center">
                <div className="action">
                  <Button
                    basic
                    color='teal'
                    onClick={generateTree}
                    disabled={workInProgress}
                    id='generateButton'
                  >
                    Generál
                  </Button>
                </div>
                {startEndButton}
              </div>
            </Segment>
            {nextButton}
          </Segment.Group>
        </Grid.Column>
        <Grid.Column id="tree" className="tree" width='9'>
          <Grid.Row>
            <div style={{ height: '70px' }} >
              <ActionMessage text={actionText} visible={actionTextVisible} />
            </div>
          </Grid.Row>
          <Grid.Row>
            {root ? (
              <ul>
                <BinarySearchTreeNode
                  node={root}
                  nodeType="root"
                  dataKey={nodeIndex}
                  swap1Index={nodesTobeSwapped.swap1 && nodesTobeSwapped.swap1.index}
                  swap2Index={nodesTobeSwapped.swap2 && nodesTobeSwapped.swap2.index}
                  maxNodeIndex={tree.length}
                  key={key}
                />
              </ul>
            ) : (
              <Segment placeholder> Fa generálásához nyomd meg a 'Generál' gombot! </Segment>
            )}
          </Grid.Row>
          <Grid.Row>
            {tree.length > 0 &&
              <Table collapsing singleLine celled>
                <Table.Body>
                  <Table.Row >
                    {treeTable}
                  </Table.Row>
                </Table.Body>
              </Table>
            }
          </Grid.Row>
        </Grid.Column>
        <Grid.Column id='algorithmColumn' width='3'>
          {algorithmTable}
        </Grid.Column>
      </Grid>
      <div style={{height: '56vh'}}></div>
      <Segment attached='bottom' inverted textAlign='center' className='footer'>
        <div>Készítette: Pácser Ildikó</div>
        <div>Kapcsolat: ildiko072[at]gmail[dot]com</div>
      </Segment>
    </React.Fragment>
  );
}

export default Animation;