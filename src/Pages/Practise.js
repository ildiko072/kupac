import React, { useContext, useState } from 'react';
import { Button, Table, Input, Grid, Popup, Segment, Modal, Icon, Header } from 'semantic-ui-react';

import { SwapNodesPractise } from '../binary-tree/contexts/SwapNodesPractise';
import "../css/BinaryNodeTreeApp.css";
import "bootstrap/dist/css/bootstrap.min.css";
import BinarySearchTreeNode from '../binary-tree/components/BinarySearchTreeNode';
import HeapSortAlgorithm from '../algorithms/HeapSortAlgorithm.js';
import BuildMaxHeapAlgorithm from '../algorithms/BuildMaxHeapAlgorithm';
import SinkAlgorithm from '../algorithms/SinkAlgorithm';
import BinarySearchTreeNodeMeta from '../binary-tree/classes/BinarySearchTreeNodeMeta';
import ActionMessage from '../messages/ActionMessage';
import GreenMessage from '../messages/GreenMessage';
import RedMessage from '../messages/RedMessage';

let currentParent = null;
let root = null;
let tree = [];
let nodeIndex = 0;
let iValue = null;
let currentIndex;
let lastIndex;
let currentSinkedIndex;
let maxHeapReady = false;
let heapSortReady = false;
let started = false;
let s1, s2;

const messageTexts = {
  notParentChild: 'Szülő-gyerek párt adj meg!',
  twoValuesNeeded: 'Adj meg két elemet a cseréhez!',
  goodJob: 'Helyes lépés!',
  tryAgain: 'Nem ez a következő lépés, próbáld újra!',
  maxHeapReady: 'Gratulálok! Elkészült a maximum kupac.',
  maxHeapReadyHitReady: 'Gratulálok! A maximum kupac elkészült. Nyomd meg a Kész gombot a rendezés módba lépéshez!',
  heapNotReadyYet: "Még nincs kész a maximum kupac, próbálkozz tovább!",
  heapSortReady: 'Gratulálok! A rendezés befejeződött.'
}

const Practise = (props) => {
  const [insertValue, setInsertValue] = useState("");
  const [actionText, setActionText] = useState("");
  const [actionTextVisible, setActionTextVisible] = useState(false);
  const [greenTextVisible, setGreenTextVisible] = useState(false);
  const [redTextVisible, setRedTextVisible] = useState(false);
  const [modeIsHeapify, setModeIsHeapify] = useState(true);
  const [modeIsHeapsort, setModeIsHeapsort] = useState(false);
  const [modeIsRandom, setModeIsRandom] = useState(true);
  const [modeIsBoth, setModeIsBoth] = useState(false);
  const [inBuildingMaxHeapMode, setInBuildingMaxHeapMode] = useState(true);
  const [inSinkMode, setInSinkMode] = useState(false);
  const [inSinkModeInSort, setInSinkModeInSort] = useState(false);
  const [inHeapSortMode, setInHeapSortMode] = useState(false);
  const nodesTobeSwapped = useContext(SwapNodesPractise);
  const [modalState, dispatch] = React.useReducer(reducer, {
    open: false
  });
  const { modalOpen } = modalState;
  const [key, setKey] = React.useReducer(c => c + 1, 0);

  const sleep = async (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  const showModal = () => {
    dispatch({ type: 'OPEN_MODAL' })
  }

  // Insert element (Elem beszúrása)
  const insert = () => {
    if (started) {
      showModal();
    } else {
      if (insertValue !== '' && tree.length < 15) {
        const newNode = new BinarySearchTreeNodeMeta(insertValue, nodeIndex);
        if (root === null) {
          root = newNode;
          currentParent = newNode;
        }
        else {
          insertNode(root, newNode);
        }
        tree = tree.concat(newNode);
        setInsertValue("");
        nodeIndex = nodeIndex + 1;
        currentIndex = tree.length - 1;
      }
    }
  };

  // Inserting element when working witg generated tree
  // (Elem beszúrása a generált fa létrehozása alatt)
  const insertGenerated = () => {
    if (iValue !== null && !isNaN(iValue) && tree.length < 15) {
      const newNode = new BinarySearchTreeNodeMeta(iValue, nodeIndex);
      if (root === null) {
        root = newNode;
        currentParent = newNode;
      }
      else {
        insertNode(root, newNode);
      }
      tree = tree.concat(newNode);
      setInsertValue("");
      nodeIndex = nodeIndex + 1;
    }
  };

  // Inserting element into the tree (Elem beszúrása a fába)
  const insertNode = (node, newNode) => {
    if (currentParent.left === null) {
      currentParent.setLeft(newNode);
    } else if (currentParent.right === null) {
      currentParent.setRight(newNode);

      if (node.height() > 1) {
        if (currentParent.parent.leftHeight() > currentParent.parent.rightHeight()) {
          currentParent = currentParent.parent.right;
          while (currentParent.left) {
            currentParent = currentParent.right;
          }
          return
        } else {
          currentParent = node.left;
          while (currentParent.left) {
            currentParent = currentParent.left;
          }
        }

        if (node.leftHeight() > node.rightHeight()) {
          currentParent = node.right;
          while (currentParent.left) {
            currentParent = currentParent.left;
          }
        }
      } else {
        currentParent = node.left;
        while (currentParent.left) {
          currentParent = currentParent.left;
        }
      }
    }
  };

  // Validating insert value (Beszúrandó elem ellenőrzése)
  const onChangeInsertValue = (e) => {
    let v = e.target.value;
    if (!isNaN(v) && v >= 0 && v < 100) {
      v = parseInt(e.target.value);
      if (!isNaN(v)) setInsertValue(v);
    }
  }

  // Clearing elements state (Elemek alap allapotba helyezese, kiveve a mar lezartat)
  const deActivate = () => {
    nodesTobeSwapped.swap1 = null;
    nodesTobeSwapped.swap2 = null;
    tree.forEach(element => { if (element.state !== 'inPlace') element.state = '' });
  }
  
  // Clearing state of all elements, including the ones already in place
  // (Összes elem alap állapotba helyezése, beleértve a már lezártakat is )
  const clearStatus = () => {
    tree.forEach(element => { element.state = '' });
  }

  // (Animacio es csere) 
  const findAndSwap = () => {
    const temp = nodesTobeSwapped.swap1.value;
    nodesTobeSwapped.swap1.value = nodesTobeSwapped.swap2.value;
    nodesTobeSwapped.swap2.value = temp;
    nodesTobeSwapped.swap1.state = 'inSwap';
    nodesTobeSwapped.swap2.state = 'inSwap';
    nodesTobeSwapped.swap1 = null;
    nodesTobeSwapped.swap2 = null;
  };

  // (Ket elem csereje a generalt maximum kupac letrehozasanal)
  const swapNodesGenerate = () => {
    if (s1 && s2) {
      const temp = s1.value;
      s1.value = s2.value;
      s2.value = temp;
      s1 = null;
      s2 = null
    }
  }

  // (Ket elem csereje)
  const swapNodes = async () => {
    if (nodesTobeSwapped.swap1 && nodesTobeSwapped.swap2) {
      if (nodesTobeSwapped.swap1.parent === nodesTobeSwapped.swap2
        || nodesTobeSwapped.swap2.parent === nodesTobeSwapped.swap1) {
        findAndSwap();
        setKey();
        await sleep(1200);
        deActivate();
        setKey();
      } else if (inHeapSortMode) {
        findAndSwap();
        setKey();
        await sleep(1200);
        deActivate();
        setKey();
      } else {
        setActionText(messageTexts.notParentChild);
        setActionTextVisible(true);
        await sleep(2000);
        setActionTextVisible(false);
      }
    } else {
      setActionText(messageTexts.twoValuesNeeded);
      setActionTextVisible(true);
      await sleep(2000);
      setActionTextVisible(false);
    }
  }

  // Checking correct to be swapped items
  // (Valos cserelendo elemek megkeresese, es kivalasztottak ellenorzese)
  const swapCheck = async (index) => {
    setActionTextVisible(false);
    let correctSwapNodes = [];
    let parent = (inBuildingMaxHeapMode) ? tree[index].parent : tree[index];
    let left = parent.left;
    let switchedNode = null;
    if (parent.right) {
      let right = parent.right;
      if (right.value > left.value) {
        if (right.value > parent.value) {
          correctSwapNodes = correctSwapNodes.concat(parent);
          correctSwapNodes = correctSwapNodes.concat(right);
          switchedNode = right;
        }
      } else if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
        switchedNode = left;
      }
    } else if (left) {
      // Only left child is present
      if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
        switchedNode = left;
      }
    }
    if (correctSwapNodes.length > 0) {
      if (correctSwapNodes.includes(nodesTobeSwapped.swap1) && correctSwapNodes.includes(nodesTobeSwapped.swap2)) {
        setGreenTextVisible(true);
        setActionText(messageTexts.goodJob);
        swapNodes();
        await sleep(2000);
        setGreenTextVisible(false);
        //check if we need to sink the switched value
        if ((switchedNode.left && switchedNode.left.value > switchedNode.value) || (switchedNode.right && switchedNode.right.value > switchedNode.value)) {
          setInBuildingMaxHeapMode(false);
          setInSinkMode(true);
          currentSinkedIndex = switchedNode.index;
        } else {
          //no more sink needed, switching back to building max heap mode
          setInBuildingMaxHeapMode(true);
          setInSinkMode(false);
        }
      } else {
        setRedTextVisible(true);
        setActionText(messageTexts.tryAgain);
        await sleep(2000);
        setRedTextVisible(false);
      }
    } else {
      if (currentIndex > 2) {
        if (currentIndex % 2 === 0) {
          currentIndex = currentIndex - 2;
          swapCheck(currentIndex);
        } else {
          currentIndex = currentIndex - 1;
          swapCheck(currentIndex);
        }
      } else if (inSinkMode && currentIndex > 0) {
        currentIndex = currentIndex - 1;
        swapCheck(currentIndex);
      } else {
        if (modeIsBoth) {
          deActivate();
          setKey();
          setGreenTextVisible(true);
          setActionText(messageTexts.maxHeapReadyHitReady);
        } else {
          setGreenTextVisible(true);
          setActionText(messageTexts.maxHeapReady);
          maxHeapReady = true;
          deActivate();
          setKey();
        }
       
      }
    }
  }

  // Cheking for next step during building max heap
  // (Kovetkezo lepes szamolasa a maximum kupac epitese soran)
  const whatIsNext = async (index, caller) => {
    setGreenTextVisible(false);
    setRedTextVisible(false);
    if (index === 0) {
      maxHeapReady = true;
      setGreenTextVisible(true);
      setActionText(messageTexts.maxHeapReady);
      setInBuildingMaxHeapMode(false);
      if (modeIsBoth) {
        setInHeapSortMode(true);
      }
      return;
    }
    let correctSwapNodes = [];
    let parent = (inBuildingMaxHeapMode) ? tree[index].parent : tree[index];
    let left = parent.left;
    // let switchedNode = null;
    if (parent.right && parent.right.state !== 'inPlace') {
      let right = parent.right;
      if (right.value > left.value) {
        if (right.value > parent.value) {
          correctSwapNodes = correctSwapNodes.concat(parent);
          correctSwapNodes = correctSwapNodes.concat(right);
        }
      } else if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
      }
    } else if (left && left.state !== 'inPLace') {
      // Only left child is present
      if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
      }
    }
    if (caller === "help" && correctSwapNodes.length > 0) {
      //next elements found
      setActionText("Ezt a két elemet cseréld meg: " + correctSwapNodes[0].value + " - " + correctSwapNodes[1].value);
      setActionTextVisible(true);
      await sleep(3000);
      setActionTextVisible(false);
    } else if (caller === "ready" && correctSwapNodes.length > 0) {
      setActionText(messageTexts.heapNotReadyYet);
      setActionTextVisible(true);
      await sleep(3000);
      setActionTextVisible(false);
    } else {
      if (index > 0) {
        whatIsNext(index - 1, caller);
      }
    }
  }

  // Sink during sorting, after swap was hit
  // Elemek csereje soran sullyesztes a rendezes alatt
  const sinkInSort = async (index) => {
    setActionTextVisible(false);
    let correctSwapNodes = [];
    let parent = tree[index];
    let left = parent.left;
    let switchedNode = null;
    let b = true;
    let i = tree.length - 1;
    while (b) {
      if (tree[i].state === 'inPlace') {
        i = i - 1;
      } else {
        b = false;
        lastIndex = i;
      }
    }
    if (parent.right && parent.right.state !== 'inPlace') {
      let right = parent.right;
      if (right.value > left.value) {
        if (right.value > parent.value) {
          correctSwapNodes = correctSwapNodes.concat(parent);
          correctSwapNodes = correctSwapNodes.concat(right);
          switchedNode = right;
        }
      } else if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
        switchedNode = left;
      }
    } else if (left && left.state !== 'inPlace') {
      // Only left child is present
      if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
        switchedNode = left;
      }
    }
    if (correctSwapNodes.length > 0) {
      if (correctSwapNodes.includes(nodesTobeSwapped.swap1) && correctSwapNodes.includes(nodesTobeSwapped.swap2)) {
        setGreenTextVisible(true);
        setActionText(messageTexts.goodJob);
        swapNodes();
        await sleep(2000);
        setGreenTextVisible(false);
        //check if we need to sink further
        if ((switchedNode.left && switchedNode.left.state !== "inPlace" && switchedNode.left.value > switchedNode.value) || (switchedNode.right && switchedNode.right.state !== "inPlace" && switchedNode.right.value > switchedNode.value)) {
          currentSinkedIndex = switchedNode.index;
        } else {
          //no more sink needed, switching back to sort mode
          setInHeapSortMode(true);
          setInSinkModeInSort(false);
        }
      } else {
        setRedTextVisible(true);
        setActionText(messageTexts.tryAgain);
        await sleep(2000);
        setRedTextVisible(false);
      }
    } else {
      //No more sink needed, setting back to sorting mode
      setInSinkModeInSort(false);
      setInHeapSortMode(true);
    }
  }

  // Checking for next step during sinking in sorting mode
  // (Kovetkezo lepes kiszamolasa a rendezes alatti sullyesztesnel)
  const whatIsNextSortSink = async (index) => {
    setGreenTextVisible(false);
    setRedTextVisible(false);
    let correctSwapNodes = [];
    let parent = tree[index];
    let left = parent.left;
    let b = true;
    let i = tree.length - 1;
    while (b) {
      if (tree[i].state === 'inPlace') {
        i = i - 1;
      } else {
        b = false;
        lastIndex = i;
      }
    }
    if (parent.right && parent.right.state !== 'inPlace') {
      let right = parent.right;
      if (right.value > left.value) {
        if (right.value > parent.value) {
          correctSwapNodes = correctSwapNodes.concat(parent);
          correctSwapNodes = correctSwapNodes.concat(right);
        }
      } else if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
      }
    } else if (left && left.state !== 'inPlace') {
      // Only left child is present
      if (left.value > parent.value) {
        correctSwapNodes = correctSwapNodes.concat(parent);
        correctSwapNodes = correctSwapNodes.concat(left);
      }
    }
    if (correctSwapNodes.length > 0) {
      setActionText("Ezt a két elemet cseréld meg: " + correctSwapNodes[0].value + " - " + correctSwapNodes[1].value);
      setActionTextVisible(true);
      await sleep(3000);
      setActionTextVisible(false);
    }
  }

  // After hitting swap button during sorting, checking if correct elemens were included
  // (Csere gombra nyomas rendezes mod alatt, helyes elemek megadasanak ellenorzese)
  const swapCheckInSort = async () => {
    setActionTextVisible(false);
    let b = true;
    let i = tree.length - 1;
    while (b) {
      if (tree[i].state === 'inPlace') {
        i = i - 1;
      } else {
        b = false;
        lastIndex = i;
      }
    }
    let correctSwapNodes = [];
    if (inHeapSortMode) {
      let lastNode = tree[lastIndex];
      let firstNode = tree[0];
      correctSwapNodes = correctSwapNodes.concat(lastNode, firstNode);

      if (correctSwapNodes.includes(nodesTobeSwapped.swap1) && correctSwapNodes.includes(nodesTobeSwapped.swap2)) {
        setGreenTextVisible(true);
        setActionText(messageTexts.goodJob);
        swapNodes();
        await sleep(1000);
        setGreenTextVisible(false);
        tree[lastIndex].state = 'inPlace';

        if (lastIndex === 1) {
          await sleep(1000);
          setGreenTextVisible(true);
          setActionText(messageTexts.heapSortReady);
          tree[0].state = 'inPlace';
          heapSortReady = true;
          setInSinkModeInSort(false);
          setInHeapSortMode(false);
        } else if ((tree[0].value < tree[0].left.value) || (tree[0].right.state !== 'inPlace' && (tree[0].value < tree[0].right.value))) {
          setInHeapSortMode(false);
          setInSinkModeInSort(true);
          currentSinkedIndex = 0;
        }
      } else {
        setRedTextVisible(true);
        setActionText(messageTexts.tryAgain);
        await sleep(2000);
        setRedTextVisible(false);
      }
    } else if (inSinkMode) {
      sinkInSort(currentSinkedIndex);
    } else if (heapSortReady) {
      setActionText(messageTexts.heapSortReady);
      setGreenTextVisible(true);
    }
  }

  // In help checking for next step during sorting
  // (Segitsegre nyomasnal kovetkezo lepes kiszamolasa a rendezes soran)
  const whatIsNextSort = async () => {
    setGreenTextVisible(false);
    setRedTextVisible(false);
    let b = true;
    let i = tree.length - 1;
    while (b) {
      if (tree[i].state === 'inPlace') {
        i = i - 1;
      } else {
        b = false;
        lastIndex = i;
      }
    }
    let correctSwapNodes = [];
    if (inHeapSortMode) {
      let lastNode = tree[lastIndex];
      let firstNode = tree[0];
      correctSwapNodes = correctSwapNodes.concat(lastNode, firstNode);

      if (correctSwapNodes.length > 0) {
        setActionText("Ezt a két elemet cseréld meg: " + correctSwapNodes[0].value + " - " + correctSwapNodes[1].value);
        setActionTextVisible(true);
        await sleep(3000);
        setActionTextVisible(false);

        if (lastIndex === 1) {
          setGreenTextVisible(true);
          setActionText(messageTexts.heapSortReady);
          tree[0].state = 'inPlace';
          heapSortReady = true;
        }
      }
    } else if (inSinkModeInSort) {
      whatIsNextSortSink(currentSinkedIndex, 'help');
    } else if (heapSortReady) {
      setActionText(messageTexts.heapSortReady);
      setGreenTextVisible(true);
    }
  }

  // Called when swap button was hit, checking mode and selected elements 
  // (Cserel gomb megnyomasakor mod es cserelendo elemek ellenorzese)
  const swapHit = async () => {
    started = true;
    setRedTextVisible(false);
    setGreenTextVisible(false);
    setActionTextVisible(false);
    if ((modeIsHeapify || modeIsBoth) && (!currentIndex || currentIndex < 0)) {
      currentIndex = tree.length - 1;
    }
    if (nodesTobeSwapped.swap1 && nodesTobeSwapped.swap2) {
      if (inBuildingMaxHeapMode) {
        swapCheck(currentIndex);
      } else if (inSinkMode) {
        swapCheck(currentSinkedIndex);
      } else if (inHeapSortMode) {
        swapCheckInSort();
      } else if (inSinkModeInSort) {
        sinkInSort(currentSinkedIndex);
      }
    } else {
      setActionText(messageTexts.twoValuesNeeded);
      setRedTextVisible(true);
      await sleep(2000);
      setRedTextVisible(false);
    }
  }

  // Setting parameters when max heap building mode is selected
  // (Kupacolas mod valasztasa eseten a parameterek beallitasa)
  const buildMaxHeap = () => {
    setModeIsHeapify(true);
    setModeIsBoth(false);
    setModeIsHeapsort(false);
    setInBuildingMaxHeapMode(true);
    setInHeapSortMode(false);
    setInSinkMode(false);
    deleteTree();
    if (tree.length > 0) {
      currentIndex = tree.length - 1;
    }
  }

  // Setting parameters when sorting mode is selected 
  // (Kupacrendezes mod valasztasa eseten a parameterek beallitasa)
  const heapSort = async () => {
    setModeIsHeapsort(true);
    setModeIsHeapify(false);
    setModeIsBoth(false);
    setInBuildingMaxHeapMode(false);
    setInSinkMode(false);
    setInHeapSortMode(true);
    deleteTree();
    if (tree.length > 0) {
      let b = true;
      let i = tree.length - 1;
      while (b) {
        if (tree[i].state === 'inPlace') {
          i = i - 1;
        } else {
          b = false;
          currentIndex = i;
        }
      }
    }
  }

  // Setting parameters when full practise mode is selected
  // (Teljes mod valasztasa eseten a parameterek beallitasa)
  const buildBoth = () => {
    setModeIsBoth(true);
    setModeIsHeapsort(false);
    setModeIsHeapify(false);
    setInBuildingMaxHeapMode(true);
    setInHeapSortMode(false);
    setInSinkMode(false);
    deleteTree();
    if (tree.length > 0) {
      currentIndex = tree.length - 1;
    }
  }

  // Called when ready button was hit by user
  // (Kesz gombra kattintasnal hivodik meg)
  const checkReady = () => {
    setActionTextVisible(false);
    setGreenTextVisible(false);
    setRedTextVisible(false);
    if (tree.length > 0) {
      started = true;
      if (!currentIndex || currentIndex < 0) currentIndex = tree.length - 1;
      if (inBuildingMaxHeapMode) {
        whatIsNext(currentIndex, 'ready');
      } else if (inSinkMode) {
        whatIsNext(currentSinkedIndex, 'ready');
      }
    }
  }

  // (Segitseg gombra kattintas)
  const help = () => {
    if (modeIsHeapify || (modeIsBoth && !maxHeapReady)) {
      let i = tree.length - 1;
      whatIsNext(i, 'help');
    } else if (modeIsHeapsort || (modeIsBoth && maxHeapReady)) {
      whatIsNextSort();
    }
  }

  // Sink element for generated max heap building
  // (Sullyesztes a generalt maximum kupac epitesehez)
  const sinkInGenerate = (index, lastIndex) => {
    let i = index;
    let j = tree[index].left && tree[index].left.index;
    let b = true;

    while (j && j <= lastIndex && b) {
      if (j < lastIndex && tree[j + 1].value > tree[j].value) {
        j = j + 1;
      }
      s1 = tree[i];
      s2 = tree[j];
      if (tree[i].value < tree[j].value) {
        swapNodesGenerate();
        i = j;
        j = tree[j].left ? tree[j].left.index : null;
      } else {
        b = false;
      }
    }
  }

  // Generated max heap building (Generalt maximum kupac epitese)
  const buildGeneratedMaxHeap = (index) => {
    s1 = tree[index];
    let switchedIndex = null;
    let thirdNode = null;
    s2 = s1.parent;
    if (s2.left !== s1) {
      thirdNode = s2.left
      if (s1.value > s2.value) {
        switchedIndex = s1.index;
        if (thirdNode.value > s1.value) {
          s1 = thirdNode;
          switchedIndex = s1.index;
        }
        swapNodesGenerate();
        sinkInGenerate(switchedIndex, tree.length - 1);
      } else {
        if (thirdNode.value > s2.value) {
          s1 = thirdNode;
          switchedIndex = s1.index;
          swapNodesGenerate();
          sinkInGenerate(switchedIndex, tree.length - 1);

        }
      }
    } else {
      if (s1.value > s2.value) {
        switchedIndex = s1.index;
        swapNodesGenerate();
        sinkInGenerate(switchedIndex, tree.length - 1);
      }
    }
    if (index > 2) {
      if (index % 2 === 0) {
        buildGeneratedMaxHeap(index - 2);
      } else {
        buildGeneratedMaxHeap(index - 1);
      }
    } else {
      s1 = null;
      s2 = null;
      maxHeapReady = true;
    }
  }


  const generateTree = () => {
    deleteTree();
    let i = Math.floor(Math.random() * 12) + 4;
    setKey();
    while (i > 0) {
      iValue = (Math.floor(Math.random() * 100));
      insertGenerated();
      i--;
    }
    // dummyTree();
    currentIndex = tree.length - 1;
    setInSinkMode(false);
    setInSinkModeInSort(false);
    // When in sorting mode, we need to generate a heap to start with
    // (Rendezes modban rogton maximum kupacot kell generalnunk)
    if (modeIsHeapify) {
      setInBuildingMaxHeapMode(true);
      setInHeapSortMode(false);
    } else if (modeIsHeapsort) {
      setInHeapSortMode(true);
      buildGeneratedMaxHeap(currentIndex);
      setInBuildingMaxHeapMode(false);
    } else if (modeIsBoth) {
      setInBuildingMaxHeapMode(true);
      setInHeapSortMode(false);
    }
    setKey();
  }

  const deleteTree = () => {
    setKey();
    started = false;
    setInSinkMode(false);
    setGreenTextVisible(false);
    setRedTextVisible(false);
    maxHeapReady = false;
    heapSortReady = false;
    nodesTobeSwapped.swap1 = null;
    nodesTobeSwapped.swap2 = null;
    root = null;
    tree = [];
    nodeIndex = 0;

    if(modeIsBoth) {
      setInBuildingMaxHeapMode(true);
      setInHeapSortMode(false);
    }
  }

  // const dummyTree = () => {
  //   deleteTree();
  //   let t = [15, 99, 45, 24, 7, 11, 35, 48, 22, 10, 1];
  //   setKey();
  //   t.forEach(e => {
  //     iValue = e;
  //     insertGenerated();
  //   })
  // }

  const setRandomMode = () => {
    setModeIsRandom(true);
    deleteTree();
  }

  const setManualMode = () => {
    setModeIsRandom(false);
    deleteTree();
  }

  let switchedCellStyle = { border: '1px solid orange', color: 'orange' }
  let inPlaceCellStyle = { background: 'lightgrey' }

  let treeTable = [];
  /* eslint-disable */
  tree.map((data) => {
    switch (data.state) {
      case "inSwap":
        treeTable.push(
          <Table.Cell key={data.index} style={switchedCellStyle} textAlign='center'>
            {data.value}
          </Table.Cell>)
        break;
      case "inPlace":
        treeTable.push(
          <Table.Cell key={data.index} style={inPlaceCellStyle} textAlign='center'>
            {data.value}
          </Table.Cell>)
        break;
      default:
        treeTable.push(
          <Table.Cell key={data.index} textAlign='center'>
            {data.value}
          </Table.Cell>)
    }
  })

  let algorithmTable = null;
  let readyButton = null;
  let generateTreeButton = null;
  let modeSelector = null;

  if (modeIsHeapify === true) {
    if (tree.length > 2 && inBuildingMaxHeapMode) {
      algorithmTable = <Segment><BuildMaxHeapAlgorithm /></Segment>;
    } else if (tree.length > 2 && inSinkMode) {
      algorithmTable = <Segment><SinkAlgorithm /></Segment>;
    }
    readyButton =
      <Popup content='Nyomd meg, ha elkészült a maximum kupac' trigger={
        <Button
          basic
          color='orange'
          onClick={checkReady}
          id="action-button"
          disabled={tree.length < 3}
        >
          Kész
        </Button>
        }
      />;

    modeSelector =
      <div className="center">
        <div id="mode">Válassz fa építési módot:</div>
        <Popup content='Véletlenszerű fa generálása' trigger={
          <Button
            toggle
            active={modeIsRandom}
            onClick={setRandomMode}
            className={"activeButton"}
          >
            Generál
          </Button>}
        />
        <Popup content='Fa elemeinek megadása' trigger={
          <Button
            toggle
            active={!modeIsRandom}
            onClick={setManualMode}
            className={"activeButton"}
          >
            Épít
          </Button>}
        />
      </div>
  } else if (modeIsHeapsort) {
    if (tree.length > 2) {
      algorithmTable = <Segment><HeapSortAlgorithm inHeapSortMode={inHeapSortMode} inSinkModeInSort={inSinkModeInSort} /></Segment>;
    }
  } else if (modeIsBoth) {
    if (inBuildingMaxHeapMode || inSinkMode) {
      readyButton =
        <Button
          basic
          color='orange'
          onClick={checkReady}
          id="action-button"
          disabled={tree.length < 3}
        >
          Kész
        </Button>
      if (tree.length > 2 && inBuildingMaxHeapMode) {
        algorithmTable = <Segment><BuildMaxHeapAlgorithm /></Segment>;
      } else if (tree.length > 2 && inSinkMode) {
        algorithmTable = <Segment><SinkAlgorithm /></Segment>;
      }
    } else {
      if (tree.length > 2) {
        algorithmTable = <Segment><HeapSortAlgorithm inHeapSortMode={inHeapSortMode} inSinkModeInSort={inSinkModeInSort} /></Segment>;
      }
    }
    modeSelector =
      <div>
        <div className="center">
          <div id="mode">Válassz fa építési módot:</div>
          <Popup content='Véletlenszerű fa generálása' trigger={
            <Button
              toggle
              active={modeIsRandom}
              onClick={setRandomMode}
              className={"activeButton"}
            >
              Generál
            </Button>}
          />
          <Popup content='Fa elemeinek megadása' trigger={
            <Button
              toggle
              active={!modeIsRandom}
              onClick={setManualMode}
              className={"activeButton"}
            >
              Épít
            </Button>}
          />
        </div>
      </div>
  }

  if (modeIsHeapsort || modeIsRandom) {
    generateTreeButton =
      <div className="center"><div className="action"><Popup content='Véletlen elemekkel feltöltött fa generálása' trigger={<Button basic color='teal' onClick={generateTree}>Kezdés</Button>} /></div></div>
  } else if (!modeIsHeapsort && !modeIsRandom) {
    generateTreeButton =
      <div className="center">
        <div className="action">
          <Input
            value={insertValue}
            onChange={onChangeInsertValue}
            type="number"
            min="0" max="99" step="1"
          />
          <Button
            basic
            color='teal'
            onClick={insert}
            disabled={tree.length > 14}
            id="action-button"
          >
            Hozzáad
          </Button>
        </div>
      </div>;
  }

  /* eslint-enable */

  function reducer(modalState, action) {
    switch (action.type) {
      case 'OPEN_MODAL':
        return { modalOpen: true }
      case 'CLOSE_MODAL':
        return { modalOpen: false }
      default:
        throw new Error()
    }
  }
  
  const modalBack = () => {
    dispatch({ type: 'CLOSE_MODAL' });
    setGreenTextVisible(false);
    setInBuildingMaxHeapMode(true);
    setInHeapSortMode(false);
    setInSinkMode(false);
    started = false;
    setInSinkMode(false);
    maxHeapReady = false;
    heapSortReady = false;
    clearStatus();
    insert();
  }

  const modalNewTree = () => {
    dispatch({ type: 'CLOSE_MODAL' });
    setGreenTextVisible(false);
    setInBuildingMaxHeapMode(true);
    setInHeapSortMode(false);
    setInSinkMode(false);
    started = false;
    deleteTree();
    clearStatus();
    insert();
  }
  
  const notificationModal = 
    <div>
      <Modal
        dimmer={'inverted'}
        open={modalOpen}
        centered
        onClose={() => dispatch({ type: 'CLOSE_MODAL' })}
      >
        <Modal.Header>Újrakezdés</Modal.Header>
        <Modal.Content>
          Elem beszúrásával újrakezdődik a kupacolás.
          Hozzáadhatod a jelenlegi fához, vagy új fát hozhatsz létre.
        </Modal.Content>
        <Modal.Actions>
          <Button color={'teal'} onClick={modalBack}>
            Hozzáadás
          </Button>
          <Button color={'green'} onClick={modalNewTree}>
            Új fa
          </Button>
        </Modal.Actions>
      </Modal>
    </div>

  return (
    <React.Fragment>
      {notificationModal}
      <Grid id="app">
        <Grid.Column id="basic-actions" width='4'>
          <Segment.Group>
            <Segment className='actionSegment'>
              <div className="center">
                <div id="mode">Válassz módot a gyakorláshoz:</div>
                <div>
                  <Popup content='Maximum kupac építése' trigger={
                    <Button
                      toggle
                      active={modeIsHeapify}
                      onClick={buildMaxHeap}
                      className={"activeButton"}
                    >
                      Kupacol
                    </Button>}
                  />
                </div>
                <div>
                  <Popup content='Kupacrendezés gyakorlása' trigger={
                    <Button
                      toggle
                      active={modeIsHeapsort}
                      onClick={heapSort}
                      className={"activeButton"}
                    >
                      Kupacrendezés
                    </Button>}
                  />
                </div>
                <div>
                  <Popup content='Kupacolás és kupacrendezés gyakorlása' trigger={
                    <Button
                      toggle
                      active={modeIsBoth}
                      onClick={buildBoth}
                      className={"activeButton"}
                    >
                      Teljes
                    </Button>}
                  />
                </div>
              </div>
              {modeSelector}
              {generateTreeButton}
            </Segment>
            <Segment>
              <Popup wide trigger={<Icon size='small' name='question'/>} >
                <Segment basic>
                  <Header>Cserél</Header>
                  Kattintással válassz ki két elemet cserére, majd nyomd meg a Cserél gombot!
                </Segment>
                <Segment basic>
                  <Header>Segítség</Header>
                  A következő lépésben cserélendő két elem kulcsának megjelenítése.
                </Segment>
                <Segment basic>
                  <Header>Kész</Header>
                  Ha elkészült a maximum kupac, a Kész gombbal ellenőrizheted. Illetve Teljes módban így tudsz továbblépni a rendezés ellenőrzésére.
                </Segment>
              </Popup>
              <div className="leftPadding">
                <div>
                  <Button
                    basic
                    color='green'
                    onClick={swapHit}
                    id="action-button"
                    disabled={tree.length < 3}
                  >
                    Cserél
                  </Button>
                </div>
                <div className="action">
                  <Popup content='Segítség a következő lépéshez' trigger={
                    <Button
                      basic
                      color='purple'
                      onClick={help}
                      id="action-button"
                      disabled={tree.length < 3 || heapSortReady}
                    >
                      Segítség
                    </Button>
                    } 
                  />
                </div>
                <div className="action">
                  {readyButton}
                </div>
              </div>
            </Segment>
          </Segment.Group>
        </Grid.Column>
        <Grid.Column id="tree" className="tree" width='9'>
          <Grid.Row>
            <div style={{ height: '70px', width: '100vmax', textAlign: 'center' }} >
              <ActionMessage text={actionText} visible={actionTextVisible} />
              <GreenMessage text={actionText} visible={greenTextVisible} />
              <RedMessage text={actionText} visible={redTextVisible} />
            </div>
          </Grid.Row>
          <Grid.Row>
            {root ? (
              <ul>
                <BinarySearchTreeNode
                  node={root}
                  nodeType="root"
                  dataKey={nodeIndex}
                  swap1Index={nodesTobeSwapped.swap1 && nodesTobeSwapped.swap1.index}
                  swap2Index={nodesTobeSwapped.swap2 && nodesTobeSwapped.swap2.index}
                  maxNodeIndex={tree.length}
                  key={key}
                />
              </ul>
            ) : (
              <Segment placeholder> Fa generálásához nyomd meg a 'Kezdés' gombot, vagy építs egy saját fát! </Segment>
            )}
          </Grid.Row>
          <Grid.Row>
            {tree.length > 0 &&
              <Table collapsing singleLine celled>
                <Table.Body>
                  <Table.Row >
                    {treeTable}
                  </Table.Row>
                </Table.Body>
              </Table>
            }
          </Grid.Row>
        </Grid.Column>
        <Grid.Column id='algorithmColumn' width='3'>
          {algorithmTable}
        </Grid.Column>
      </Grid>
      <div style={{height: '45vh'}}></div>
      <Segment attached='bottom' inverted textAlign='center' className='footer'>
        <div>Készítette: Pácser Ildikó</div>
        <div>Kapcsolat: ildiko072[at]gmail[dot]com</div>
      </Segment>
    </React.Fragment>
  );
}

export default Practise;