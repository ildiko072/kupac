import React, { Component } from "react";
import { Grid, Header, Segment } from 'semantic-ui-react';
import heapPicture from '../pictures/kupac.png';
import heapifyGif from '../pictures/kupacolGif.gif';
import sortGif from '../pictures/kRendezes.gif';
import SinkAlgorithm from "../algorithms/SinkAlgorithm";
import background from '../pictures/bgr4.bmp';

class Concept extends Component {
  render() {
    return (
      <Grid
        style={
          {
            backgroundImage: "url(" + background + ")",
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            height: '100%'
          }}
      >
        <Grid.Row>
          <Grid.Column width='16'>
            <Header className='whiteHeader'>
              Kupacrendezés - fogalmak
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row id='concept' >
          <Grid.Column width='1'></Grid.Column>
          <Grid.Column width='7'>
            <Segment compact floated='right' color='blue'>
              <img src={heapPicture} alt="heapPicture" className='noOp' />
            </Segment>
          </Grid.Column>
          <Grid.Column width='7' textAlign='rigth'>
            <Segment.Group raised compact id='conceptSegment' className='maxWidth'>
              <Segment padded inverted color='blue' size='large'> <Header>Kupac</Header>
                  A kupacrendezés egy helyben rendező algoritmus, amely a kupac adatszerkezetet veszi alapul a rendezéshez.<br />
                  A kupac egy majdnem teljes bináris fa, tehát minden szintje teljes, kivéve a legalacsonyabbat ahol balról jobbra haladva csak egy adott csúcsig vannak elemek.
              </Segment>
              <Segment padded inverted color='blue' size='large'>
                Az ábrán egy kupacot láthatunk, a föléjük írt számok a csúcsoknak megfelelő tömbelem indexei. Alul a tömbös ábrázolás látható. <br />
                A tömb alatti és feletti ívek a szülő-gyerek viszonyokat mutatják, a szülő mindig balra található a gyerekhez képest.
              </Segment>
              <Segment padded inverted color='blue' size='large'> <Header>Maximum kupac</Header>
                Mielőtt a kupacrendezést elkezdhetnénk, ki kell alakítanunk a maximum kupacot.<br />
                Azt a bináris fát nevezzük maximum kupacnak, melyben minden belső csúcs kulcsa nagyobb-egyenlő, mint a gyerekeié.<br />
                Egy ilyet láthatunk ezen az ábrán.
              </Segment>
            </Segment.Group>
          </Grid.Column>
          <Grid.Column width='1'></Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width='1'></Grid.Column>
          <Grid.Column width='14'></Grid.Column>
          <Grid.Column width='1'></Grid.Column>
        </Grid.Row>
        <Grid.Row id='concept'>
          <Grid.Column width='16'>
            <Header className='whiteHeader'>Maximum kupac építése, a kupacolás</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row id='concept'>
          <Grid.Column width='1'></Grid.Column>
          <Grid.Column width='7' floated='right'>
            <Segment padded inverted color='blue' size='large' floated='right' className='raisedSegment'><Header>Kupacolás</Header>
              A maximum kupac kialakításának folyamatát nevezzük kupacolásnak.
              Az utolsó csúcstól kezdve, szintfolytonasan visszafelé haladva a gyökér csúcsot lesüllyesztjük amikor szükséges, ezzel kialakítva a kupacot.
              Miután az alsó szinttel végeztünk, szintenként a gyökér felé haladva állítjuk helyre a csonka kupacokat.
            </Segment>
          </Grid.Column>
          <Grid.Column width='7'>
            <Segment color='blue' compact > <img src={heapifyGif} alt="heapGif" className='noOp' /></Segment>
          </Grid.Column>
          <Grid.Column width='1'></Grid.Column>
        </Grid.Row>
        <Grid.Row id='concept' className='margintop'>
          <Grid.Column width='1'></Grid.Column>
          <Grid.Column width='7'>
            <Segment padded inverted color='blue' size='large' floated='right' className='raisedSegment'>
              A süllyesztés algoritmusa mind a kupacolás, mind a kupacrendezés alatt meghívódik. Összehasonlítja az épp aktuális részfa elemeit, és amikor a gyerek kulcsa nagyobb,
              mint a szülőé, akkor megcseréli őket.
            </Segment>
          </Grid.Column>
          <Grid.Column width='7'>
            <Segment color='blue' className='maxWidth'><SinkAlgorithm noColor='true' /></Segment>
          </Grid.Column>
          <Grid.Column width='1'></Grid.Column>
        </Grid.Row>
        <Grid.Row id='concept'>
          <Grid.Column width='16'>
            <Header className='whiteHeader'>Kupacrendezés</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row id='concept'>
          <Grid.Column width='1'></Grid.Column>
          <Grid.Column width='7'>
            <Segment color='blue' floated='right' compact ><img src={sortGif} alt="sortGif" className='noOp' /> </Segment>
          </Grid.Column>
          <Grid.Column width='7'>
            <Segment.Group raised compact id='conceptSegment'  className='maxWidth'>
              <Segment padded inverted color='blue' size='large'>
                A maximum-kupacban a legnagyobb elem a fa gyökerében helyezkedik el. Mint ahogy a gif-en látható, ezt az elemet megcseréljük a fa utolsó elemével, így a helyére kerül a tömbben.<br />
                  Ezzel a lépéssel viszont elrontottuk a kupac tulajdonságot, a gyökérben levő elemet le kell süllyesztenünk a helyére. Így a gyökér elemtől kezdve újra meghívjuk rá a kupacol eljárást.
                </Segment>
              <Segment padded inverted color='blue' size='large'>
                Miután ez az elem a helyére került, így nem fogjuk már a rendezés alatt elmozdítani, úgymond rögzítjük. Ezt jelöli az elemek szürke háttérszíne.<br />
                Innen ismét folytathatjuk a gyökér és az utolsó elem cseréjével a rendezést, mivel újra a gyökér elem a legnagyobb értékű a fában. Majd ismét rekurzívan meghívjuk a kupacol eljárást a gyökér elemre. <br />
                És így tovább, amíg az utolsó elem is a helyére nem kerül.
              </Segment>
            </Segment.Group>
          </Grid.Column>
          <Grid.Column width='1'></Grid.Column>
        </Grid.Row>
        <div></div>
        <div style={{height: '40vh'}}></div>
      <Segment attached='bottom' inverted textAlign='center' className='footer'>
        <div>Készítette: Pácser Ildikó</div>
        <div>Kapcsolat: ildiko072[at]gmail[dot]com</div>
      </Segment>
      </Grid>
    );
  }
}

export default Concept;