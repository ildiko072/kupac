import React from 'react';
import ReactDOM from 'react-dom';
import {
  Route,
  Redirect,
  HashRouter
} from 'react-router-dom';

import Animation from './Pages/Animation';
import Concept from './Pages/Concept';
import Practise from './Pages/Practise';

import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import { Segment, Header } from 'semantic-ui-react';
import AlertTemplate from 'react-alert-template-basic';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'semantic-ui-less/semantic.less';
import MenuPointing from './menu/Menu';
import { SwapNodesAnimationProvider } from './binary-tree/contexts/SwapNodesAnimation';
import { SwapNodesPractiseProvider } from './binary-tree/contexts/SwapNodesPractise';

const options = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: '30px',
  transition: transitions.SCALE
}

ReactDOM.render(
  <HashRouter>
    <React.StrictMode>
      <SwapNodesAnimationProvider>
        <SwapNodesPractiseProvider>
          <AlertProvider template={AlertTemplate} {...options}>
            <App>
              <Segment inverted color='teal'>
                <Header as='h3'>Kupacrendezés oktatóprogram</Header>
              </Segment>
              <MenuPointing />
              <Route exact path="/elmelet"
                render={() => <Concept />} />
                <Route exact path="/bemutato"
                  render={() => <Animation />} />
              <Route exact path="/gyakorlas"
                render={() => <Practise />} />
              <Route exact path="/">
                <Redirect to="/elmelet" />
              </Route>
            </App>
          </AlertProvider>
        </SwapNodesPractiseProvider>
      </SwapNodesAnimationProvider>
    </React.StrictMode>
  </HashRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
