import React from 'react';
import './App.css';
import { Container } from "semantic-ui-react";

function App ({ children }) {
  return (
    <Container fluid>
      {children}
    </Container>
  );
}

export default App;
