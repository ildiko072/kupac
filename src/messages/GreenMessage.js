import React from 'react'
import { Message } from 'semantic-ui-react'

const GreenMessage = (props) => (
  <Message color='green' hidden={!props.visible} className='animateMessage'>
    {props.text}
  </Message>
)

export default GreenMessage