import React from 'react'
import { Message } from 'semantic-ui-react'

const ActionMessage = (props) => (
  <Message hidden={!props.visible} id='actionMessage' className='animateMessage'>
    {props.text}
  </Message>
)

export default ActionMessage
