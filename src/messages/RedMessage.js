import React from 'react'
import { Message } from 'semantic-ui-react'

const RedMessage = (props) => (
  <Message color='red' hidden={!props.visible} className='animateMessage'>
    {props.text}
  </Message>
)

export default RedMessage
