import React, { useContext, useState } from 'react';
import { Button } from 'semantic-ui-react';
import Tada from 'react-reveal/Tada';
import Pulse from 'react-reveal/Pulse';
import "../../css/BinaryNodeTreeApp.css";
import { SwapNodesPractise } from '../contexts/SwapNodesPractise';
import { useLocation } from 'react-router-dom';

const BinarySearchTreeNode = (props) => {
  const [active, setActive] = useState(false);
  const nodesTobeSwapped = useContext(SwapNodesPractise);
  const location = useLocation();

  // Node selection, deselection when user clicked on it
  const activate = (e) => {
    e.preventDefault();
    if (location.pathname === "/gyakorlas") {
      if (!active) {
        if (nodesTobeSwapped.swap1 === null) {
          nodesTobeSwapped.swap1 = props.node;
          nodesTobeSwapped.swap1.state = "active";
          setActive(true);
        } else if (nodesTobeSwapped.swap2 === null) {
          nodesTobeSwapped.swap2 = props.node;
          nodesTobeSwapped.swap2.state = "active";
          setActive(true);
        }
      } else {
        setActive(false);
        if (nodesTobeSwapped.swap1 && e.currentTarget.outerText === nodesTobeSwapped.swap1.value.toString()) {
          nodesTobeSwapped.swap1.state = "";
          nodesTobeSwapped.swap1 = null;
        } else if (nodesTobeSwapped.swap2 && e.currentTarget.outerText === nodesTobeSwapped.swap2.value.toString()) {
          nodesTobeSwapped.swap2.state = "";
          nodesTobeSwapped.swap2 = null;
        }
      }
    }
  };
  
  let comp;

  // State definitions (animation type and color) for the nodes
  switch(props.node.state) {
    case "active":
      comp = 
        <Pulse>
          <Button circular size='small' basic color='black'
          // eslint-disable-next-line  
          onClick={activate} data-key={props.node.index} className={props.nodeType + (props.node.active === true ? " active" : "")} >{props.node.value}</Button>
        </Pulse>;
      break;
    case "inSwap":
      comp = 
        <Tada>
          <Button circular size='small' basic color='orange'
          // eslint-disable-next-line  
          data-key={props.node.index} className={props.nodeType + (props.node.active === true ? " active" : "")} >{props.node.value}</Button>
        </Tada>;
      break;
    case "inPlace":
      comp = 
          <Button circular size='small' color='light-grey'
          // eslint-disable-next-line  
          data-key={props.node.index} className={props.nodeType + (props.node.active === true ? " active" : "")} >{props.node.value}</Button>
      break;
    default:
      comp = 
        <Button circular size='small' basic
        // eslint-disable-next-line  
        onClick={activate} data-key={props.node.index} className={props.nodeType + (props.node.active === true ? " active" : "")} >{props.node.value}</Button>;
  }
   

  return (
    <React.Fragment>
      <li>
        {comp}
        {(props.node.left || props.node.right) &&
          <ul>
            {props.node.left &&
              <BinarySearchTreeNode data-key={props.node.index} node={props.node.left} nodeType="left" />
            }
            {props.node.right &&
              <BinarySearchTreeNode data-key={props.node.index} node={props.node.right} nodeType="right" />
            }
            {props.node.right === null &&
              <li className='empty'></li>
            }
          </ul>
        }
      </li>
    </React.Fragment>
  );
}

export default BinarySearchTreeNode;