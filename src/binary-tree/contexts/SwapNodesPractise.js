import React, { createContext, useState } from "react";

export const SwapNodesPractise = createContext();

// This context provider is used for the Practise part (Gyakorlas)
export const SwapNodesPractiseProvider = ({ children }) => {
  const [swap1, setSwap1] = useState(null);
  const [swap2, setSwap2] = useState(null);
  const [thirdNode, setThirdNode] = useState(null);

  return (
    <SwapNodesPractise.Provider
      value={{
        swap1,
        swap2,
        thirdNode,
        setSwap1,
        setSwap2,
        setThirdNode
      }}
    >
      {children}
    </SwapNodesPractise.Provider>
  );
};
