import React, { createContext, useState } from "react";

export const SwapNodesAnimation = createContext();

// This context provider is passed to any component requiring the context
// Used for Animation page (Bemutato)
export const SwapNodesAnimationProvider = ({ children }) => {
  const [swap1, setSwap1] = useState(null);
  const [swap2, setSwap2] = useState(null);
  const [thirdNode, setThirdNode] = useState(null);

  return (
    <SwapNodesAnimation.Provider
      value={{
        swap1,
        swap2,
        thirdNode,
        setSwap1,
        setSwap2,
        setThirdNode
      }}
    >
      {children}
    </SwapNodesAnimation.Provider>
  );
};
